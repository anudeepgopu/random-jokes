package com.poc.jokes

import android.app.Application
import com.poc.jokes.di.AppComponent
import com.poc.jokes.di.DaggerAppComponent
import com.poc.jokes.framework.di.RepositoryModule
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class App : Application(), HasAndroidInjector {
    private lateinit var mAppComponent: AppComponent

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>
    override fun onCreate() {
        super.onCreate()
        mAppComponent = DaggerAppComponent.builder()
            .repositoryModule(RepositoryModule(this))
            .build().apply { inject(this@App) }
    }

    fun getAppComponent(): AppComponent {
        return mAppComponent
    }

    override fun androidInjector() = dispatchingAndroidInjector
}