package com.poc.jokes.di

import com.poc.jokes.App
import com.poc.jokes.framework.di.ApiServicesModule
import com.poc.jokes.framework.di.DataSourceModule
import com.poc.jokes.framework.di.FrameworkModule
import com.poc.jokes.framework.di.RepositoryModule
import com.poc.jokes.interactor.di.DispatcherModule
import com.poc.jokes.presentation.di.ViewModelModule
import com.poc.jokes.ui.di.MainUiModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidInjectionModule::class,
        ViewModelModule::class,
        DispatcherModule::class,
        FrameworkModule::class,
        ApiServicesModule::class,
        RepositoryModule::class,
        DataSourceModule::class,
        MainUiModule::class]
)
interface AppComponent {

    fun inject(app: App)
}