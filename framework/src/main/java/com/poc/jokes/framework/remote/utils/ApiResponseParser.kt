package com.poc.jokes.framework.remote.utils

import com.poc.jokes.entities.common.AppError
import com.poc.jokes.entities.common.AppResult
import com.poc.jokes.entities.remote.ApiError
import com.squareup.moshi.Moshi
import retrofit2.Response
import java.io.IOException
import javax.inject.Inject

class ApiResponseParser @Inject constructor(private val moshi: Moshi) {

    suspend fun <T> parseResponse(execute: suspend () -> Response<T>) =
        try {
            execute.invoke().let {
                if (it.isSuccessful)
                    AppResult.Success(it.body()).castIfNull()
                else AppResult.Error(
                    moshi.adapter(ApiError::class.java)
                        .fromJson(
                            it.errorBody()?.source()!!/*Throws Error if null*/
                        )?.message.orEmpty(),
                    AppError.ApiError
                )
            }
        } catch (throwable: Throwable) {
            when (throwable) {
                is IOException -> AppResult.Error(null, AppError.ConnectionError)
                else -> AppResult.Error(null, AppError.UnknownError)
            }
        }

    private fun <T> AppResult<T?>.castIfNull(): AppResult<T?> {
        return if (this is AppResult.Success && value == null) {
            AppResult.Empty()
        } else {
            this
        }
    }
}