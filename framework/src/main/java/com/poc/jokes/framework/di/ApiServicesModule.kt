package com.poc.jokes.framework.di

import com.poc.jokes.framework.remote.services.JokesService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

// DI module for Remote
@Module
class ApiServicesModule {

    @Provides
    @Singleton
    fun provideJokesService(retrofit: Retrofit): JokesService {
        return retrofit.create(JokesService::class.java)
    }
}