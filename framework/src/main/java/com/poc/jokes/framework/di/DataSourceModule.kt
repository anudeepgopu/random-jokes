package com.poc.jokes.framework.di

import com.poc.jokes.framework.remote.datasource.RemoteJokesDataSourceImpl
import com.poc.jokes.framework.remote.services.JokesService
import com.poc.jokes.framework.remote.utils.ApiResponseParser
import com.poc.jokes.interactor.datasource.RemoteJokesDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

// DI module for Remote DataSources and Local Data Sources(If Local DB is added in the future)
@Module
class DataSourceModule {

    @Provides
    @Singleton
    fun provideJokesDataSource(
        jokesService: JokesService,
        apiResponseParser: ApiResponseParser
    ): RemoteJokesDataSource = RemoteJokesDataSourceImpl(
        jokesService,
        apiResponseParser
    )
}