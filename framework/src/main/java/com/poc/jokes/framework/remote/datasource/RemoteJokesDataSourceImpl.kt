package com.poc.jokes.framework.remote.datasource

import com.poc.jokes.entities.common.AppResult
import com.poc.jokes.entities.remote.RandomJokesResponse
import com.poc.jokes.framework.remote.services.JokesService
import com.poc.jokes.framework.remote.utils.ApiResponseParser
import com.poc.jokes.interactor.datasource.RemoteJokesDataSource
import javax.inject.Inject

internal class RemoteJokesDataSourceImpl @Inject constructor(
    private val jokesService: JokesService,
    private val apiResponseParser: ApiResponseParser
) : RemoteJokesDataSource {

    override suspend fun getRandomJokes(): AppResult<RandomJokesResponse?> =
        apiResponseParser.parseResponse {
            jokesService.getRandomJokes()
        }
}