package com.poc.jokes.framework.remote.services

import com.poc.jokes.entities.remote.JokeCategory
import com.poc.jokes.entities.remote.RandomJokesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface JokesService {

    @GET("joke/{category}")
    suspend fun getRandomJokes(
        @Path("category") category: String = JokeCategory.ANY.value,
        @Query("amount") amount: Int = 10
    ): Response<RandomJokesResponse>
}