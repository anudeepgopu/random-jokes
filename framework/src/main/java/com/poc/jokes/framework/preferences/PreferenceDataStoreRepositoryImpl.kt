package com.poc.jokes.framework.preferences

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.longPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.poc.jokes.interactor.repository.PreferenceDataStoreRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class PreferenceDataStoreRepositoryImpl(private val context: Context) :
    PreferenceDataStoreRepository {

    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")
    private val appOpenCount = longPreferencesKey("app_open_count")

    override fun geAppOpenCount(): Flow<Long> = context.dataStore.data.map { preferences ->
        preferences[appOpenCount] ?: 0
    }

    override suspend fun incrementAppOpenCount() {
        context.dataStore.edit { settings ->
            settings[appOpenCount] = (settings[appOpenCount] ?: 0) + 1
        }
    }
}