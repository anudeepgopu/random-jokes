package com.poc.jokes.framework.di

import android.app.Application
import com.poc.jokes.framework.preferences.PreferenceDataStoreRepositoryImpl
import com.poc.jokes.framework.repositories.JokesRepositoryImpl
import com.poc.jokes.interactor.datasource.RemoteJokesDataSource
import com.poc.jokes.interactor.repository.JokesRepository
import com.poc.jokes.interactor.repository.PreferenceDataStoreRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

// DI module for Repositories
@Module
class RepositoryModule(private val app: Application) {

    @Provides
    @Singleton
    fun provideJokesRepository(
        remoteJokesDataSource: RemoteJokesDataSource
    ): JokesRepository = JokesRepositoryImpl(
        remoteJokesDataSource
    )

    @Provides
    @Singleton
    fun providePreferenceDataStoreRepository(): PreferenceDataStoreRepository =
        PreferenceDataStoreRepositoryImpl(app.applicationContext)
}