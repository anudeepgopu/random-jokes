package com.poc.jokes.framework.repositories

import com.poc.jokes.entities.common.AppResult
import com.poc.jokes.entities.common.castWithValue
import com.poc.jokes.entities.common.valueOrNull
import com.poc.jokes.entities.remote.Joke
import com.poc.jokes.interactor.datasource.RemoteJokesDataSource
import com.poc.jokes.interactor.repository.JokesRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

internal class JokesRepositoryImpl @Inject constructor(
    private val remoteJokesDataSource: RemoteJokesDataSource
) : JokesRepository {

    override fun getRandomJokes(): Flow<AppResult<List<Joke>>> = flow {
        // Follow local first approach when local DB is added
        emit(AppResult.Loading())
        remoteJokesDataSource.getRandomJokes().let {
            emit(it.castWithValue(it.valueOrNull()?.jokes.orEmpty()))
        }
    }
}