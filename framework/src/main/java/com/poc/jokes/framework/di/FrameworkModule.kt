package com.poc.jokes.framework.di

import com.poc.jokes.framework.remote.ApiConfig
import com.poc.jokes.framework.remote.adapters.JokeTypeAdapter
import com.poc.jokes.framework.remote.utils.ApiResponseParser
import com.poc.jokes.frameworks.BuildConfig
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

// DI module for Framework
@Module
class FrameworkModule {

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient().newBuilder().apply {
            // Logging only for DEBUG build
            if (BuildConfig.DEBUG) {
                addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
            }
        }.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient, moshi: Moshi): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(ApiConfig.BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
    }

    @Provides
    @Singleton
    fun provideMoshi(): Moshi = Moshi.Builder().add(JokeTypeAdapter()).build()

    @Provides
    @Singleton
    fun provideApiResponseParser(moshi: Moshi): ApiResponseParser = ApiResponseParser(moshi)
}