package com.poc.jokes.framework.remote.adapters

import com.poc.jokes.entities.remote.JokeType
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

internal class JokeTypeAdapter {

    @ToJson
    fun toJson(type: JokeType): String? = type.value

    @FromJson
    fun fromJson(type: String?): JokeType =
        JokeType.values().associateBy(JokeType::value)[type] ?: JokeType.UNKNOWN
}