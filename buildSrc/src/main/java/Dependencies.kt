object Versions {

    const val kotlin_version = "1.5.31"
    const val coroutine_version = "1.4.2"
    const val appcompat_version = "1.2.0"
    const val navigation_version = "2.3.5"
    const val core_ktx_version = "1.3.2"

    const val constraint_layout_version = "2.0.4"
    const val recycler_view_version = "1.2.0"
    const val swipe_refresh_version = "1.1.0"
    const val material_design_version = "1.3.0"
    const val ext_junit_version = "1.1.2"
    const val espresso_version = "3.3.0"
    const val lifecycle_version = "2.3.1"
    const val data_store_version = "1.0.0"

    const val dagger_version = "2.35"
    const val retrofit_version = "2.9.0"
    const val okhttp_logging_version = "4.9.1"
    const val moshi_version = "1.12.0"
    const val flexbox_version = "3.0.0"
}

object TestVersions {
    const val junit_version = "4.12"
    const val mockito_version = "3.2.0"
}

object KotlinDependencies {
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin_version}"
    const val coroutineAndroid =
        "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutine_version}"
    const val coroutineCore =
        "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutine_version}"
}

object AndroidxSupportDependencies {
    const val navigationFragments = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation_version}"
    const val navigationUi = "androidx.navigation:navigation-ui-ktx:${Versions.navigation_version}"
    const val appcompat = "androidx.appcompat:appcompat:${Versions.appcompat_version}"
    const val coreKtx = "androidx.core:core-ktx:${Versions.core_ktx_version}"
    const val constraintLayout =
        "androidx.constraintlayout:constraintlayout:${Versions.constraint_layout_version}"
    const val recyclerview = "androidx.recyclerview:recyclerview:${Versions.recycler_view_version}"
    const val swipeRefresh =
        "androidx.swiperefreshlayout:swiperefreshlayout:${Versions.swipe_refresh_version}"
    const val material = "com.google.android.material:material:${Versions.material_design_version}"
    const val extJunit = "androidx.test.ext:junit:${Versions.ext_junit_version}"
    const val espresso = "androidx.test.espresso:espresso-core:${Versions.espresso_version}"
    const val lifeCycle =
        "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifecycle_version}"
    const val lifeCycleViewModel =
        "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle_version}"
    const val dataStore =
        "androidx.datastore:datastore-preferences:${Versions.data_store_version}"
}

object ExternalDependencies {
    const val dagger = "com.google.dagger:dagger:${Versions.dagger_version}"
    const val daggerAndroid = "com.google.dagger:dagger-android:${Versions.dagger_version}"
    const val daggerAndroidSupport = "com.google.dagger:dagger-android-support:${Versions.dagger_version}"
    const val daggerCompile = "com.google.dagger:dagger-compiler:${Versions.dagger_version}"
    const val daggerAnnotationProcessor =
        "com.google.dagger:dagger-android-processor:${Versions.dagger_version}"
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit_version}"
    const val okHttpLoggingInterceptor =
        "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp_logging_version}"
    const val moshi = "com.squareup.moshi:moshi:${Versions.moshi_version}"
    const val moshiCodegen = "com.squareup.moshi:moshi-kotlin-codegen:${Versions.moshi_version}"
    const val flexBox = "com.google.android.flexbox:flexbox:${Versions.flexbox_version}"
    const val moshiRetrofitConverter =
        "com.squareup.retrofit2:converter-moshi:${Versions.retrofit_version}"
}

object TestDependencies {
    const val junit = "junit:junit:${TestVersions.junit_version}"
    const val mockito = "org.mockito.kotlin:mockito-kotlin:${TestVersions.mockito_version}"
    const val coroutineTest =
        "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutine_version}"
}

object Modules {
    const val ui_main = ":ui_main"
    const val framework = ":framework"
    const val interactor = ":interactor"
    const val entities = ":entities"
    const val presentation = ":presentation"
}