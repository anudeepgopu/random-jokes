package com.poc.jokes.ui.jokes

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.poc.jokes.R
import com.poc.jokes.databinding.ListItemJokeBinding
import com.poc.jokes.entities.remote.Joke
import com.poc.jokes.entities.remote.JokeType

class JokesAdapter(private val onClick: (Joke) -> Unit) :
    ListAdapter<Joke, JokesAdapter.JokeViewHolder>(DiffCallback) {

    class JokeViewHolder(val view: ListItemJokeBinding) : RecyclerView.ViewHolder(view.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JokeViewHolder {
        return JokeViewHolder(
            ListItemJokeBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: JokeViewHolder, position: Int) {
        getItem(position).apply {
            when (type) {
                JokeType.SINGLE -> holder.view.setSingleLineJoke(joke)
                JokeType.TWO_PART -> holder.view.setTwoPartJoke(setup, delivery)
                JokeType.UNKNOWN,
                null -> holder.view.setSingleLineJoke(holder.view.root.context.getString(R.string.error_displaying_joke))
            }
            holder.view.root.setOnClickListener {
                onClick.invoke(this)
            }
        }
    }

    private fun ListItemJokeBinding.setSingleLineJoke(joke: String?) {
        tvJokeSecondary.isVisible = false
        tvJokeTitle.text = joke
    }

    private fun ListItemJokeBinding.setTwoPartJoke(joke: String?, punch: String?) {
        tvJokeSecondary.isVisible = true
        tvJokeTitle.text = joke
        tvJokeSecondary.text = punch
    }

    object DiffCallback : DiffUtil.ItemCallback<Joke>() {

        override fun areItemsTheSame(oldItem: Joke, newItem: Joke) = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: Joke, newItem: Joke) = oldItem == newItem
    }
}