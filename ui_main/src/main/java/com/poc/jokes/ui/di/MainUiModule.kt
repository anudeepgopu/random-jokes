package com.poc.jokes.ui.di

import com.poc.jokes.ui.SplashActivity
import com.poc.jokes.ui.dashboard.DashboardActivity
import com.poc.jokes.ui.jokes.JokeDetailsDialogFragment
import com.poc.jokes.ui.jokes.JokesListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainUiModule {

    @ContributesAndroidInjector
    abstract fun splashActivity(): SplashActivity

    @ContributesAndroidInjector
    abstract fun dashboardActivity(): DashboardActivity

    @ContributesAndroidInjector
    abstract fun jokesListFragment(): JokesListFragment

    @ContributesAndroidInjector
    abstract fun jokeDetailsDialogFragment(): JokeDetailsDialogFragment
}