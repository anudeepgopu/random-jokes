package com.poc.jokes.ui.base

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.FrameLayout
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject


abstract class BaseDialogFragment<VM : ViewModel, VB : ViewBinding>(
    @LayoutRes layoutRes: Int,
    private val bindingFactory: (View) -> VB,
    private val modelClass: Class<VM>
) : DialogFragment(layoutRes) {

    protected var binding: VB? = null
        private set

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    protected lateinit var viewModel: VM
        private set

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(modelClass)
        val binding = bindingFactory(view)
        this.binding = binding
        onViewBindingCreated(binding, savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    @CallSuper
    protected open fun onViewBindingCreated(binding: VB, savedInstanceState: Bundle?) = Unit

    override fun onResume() {
        super.onResume()
        val params: WindowManager.LayoutParams? = dialog?.window?.attributes
        params?.width = FrameLayout.LayoutParams.MATCH_PARENT
        dialog?.window?.attributes = params as WindowManager.LayoutParams
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }
}