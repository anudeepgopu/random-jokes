package com.poc.jokes.ui.jokes

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.poc.jokes.databinding.FlexListItemJokeFlagBinding

class JokeFlagsAdapter :
    ListAdapter<String, JokeFlagsAdapter.JokeFlagViewHolder>(DiffCallback) {

    class JokeFlagViewHolder(val view: FlexListItemJokeFlagBinding) :
        RecyclerView.ViewHolder(view.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JokeFlagViewHolder {
        return JokeFlagViewHolder(
            FlexListItemJokeFlagBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: JokeFlagViewHolder, position: Int) {
        holder.view.tvFlag.text = getItem(position)
    }

    object DiffCallback : DiffUtil.ItemCallback<String>() {

        override fun areItemsTheSame(oldItem: String, newItem: String) = oldItem == newItem
        override fun areContentsTheSame(oldItem: String, newItem: String) = oldItem == newItem
    }
}