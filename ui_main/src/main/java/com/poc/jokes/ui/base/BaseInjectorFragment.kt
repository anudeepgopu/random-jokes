package com.poc.jokes.ui.base

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

abstract class BaseInjectorFragment<VM : ViewModel, VB : ViewBinding>(
    @LayoutRes layoutRes: Int,
    private val bindingFactory: (View) -> VB,
    private val modelClass: Class<VM>
) : Fragment(layoutRes) {

    protected var binding: VB? = null
        private set

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    protected lateinit var viewModel: VM
        private set

    protected val navController by lazy {
        findNavController()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this, viewModelFactory).get(modelClass)
        val binding = bindingFactory(view)
        this.binding = binding
        onViewBindingCreated(binding, savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    @CallSuper
    protected open fun onViewBindingCreated(binding: VB, savedInstanceState: Bundle?) = Unit
}