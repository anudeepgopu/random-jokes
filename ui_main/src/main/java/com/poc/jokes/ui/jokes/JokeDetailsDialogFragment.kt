package com.poc.jokes.ui.jokes

import android.os.Bundle
import androidx.core.view.isVisible
import androidx.navigation.fragment.navArgs
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.poc.jokes.R
import com.poc.jokes.databinding.DialogJokeDetailsBinding
import com.poc.jokes.entities.remote.JokeType
import com.poc.jokes.presentation.jokes.JokeDetailsDialogViewModel
import com.poc.jokes.ui.base.BaseDialogFragment

class JokeDetailsDialogFragment :
    BaseDialogFragment<JokeDetailsDialogViewModel, DialogJokeDetailsBinding>(
        R.layout.dialog_joke_details,
        DialogJokeDetailsBinding::bind,
        JokeDetailsDialogViewModel::class.java
    ) {

    private val args: JokeDetailsDialogFragmentArgs by navArgs()

    override fun onViewBindingCreated(
        binding: DialogJokeDetailsBinding,
        savedInstanceState: Bundle?
    ) {
        super.onViewBindingCreated(binding, savedInstanceState)
        binding.btnClose.setOnClickListener {
            dismiss()
        }
        args.joke.apply {
            when (type) {
                JokeType.SINGLE -> setSingleLineJoke(joke)
                JokeType.TWO_PART -> setTwoPartJoke(setup, delivery)
                JokeType.UNKNOWN,
                null -> setSingleLineJoke(getString(R.string.error_displaying_joke))
            }
            binding.tvCategory.text = getString(R.string.label_joke_category, category)
            binding.tvId.text = getString(R.string.label_joke_id, id)
            binding.tvLanguage.text = getString(R.string.label_joke_language, lang)
            binding.tvType.text = getString(R.string.label_joke_type, type?.value)
            binding.tvSafe.isVisible = safe
            viewModel.filterFlags(flags).let {
                if (it.isNotEmpty()) {
                    binding.tvFlags.isVisible = true
                    binding.rvFlags.apply {
                        layoutManager = FlexboxLayoutManager(requireContext()).also {
                            it.flexDirection = FlexDirection.ROW
                        }
                        adapter = JokeFlagsAdapter().apply {
                            submitList(it)
                        }
                    }
                }
            }
        }
    }

    private fun setSingleLineJoke(joke: String?) {
        binding?.tvJokeSecondary?.isVisible = false
        binding?.tvJokeTitle?.text = joke
    }

    private fun setTwoPartJoke(joke: String?, punch: String?) {
        binding?.tvJokeSecondary?.isVisible = true
        binding?.tvJokeTitle?.text = joke
        binding?.tvJokeSecondary?.text = punch
    }
}