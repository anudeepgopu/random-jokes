package com.poc.jokes.ui

import android.content.Intent
import android.os.Bundle
import com.poc.jokes.R
import com.poc.jokes.databinding.ActivitySplashBinding
import com.poc.jokes.presentation.SplashViewModel
import com.poc.jokes.presentation.extensions.observe
import com.poc.jokes.ui.base.BaseInjectorActivity
import com.poc.jokes.ui.dashboard.DashboardActivity

const val IS_APP_OPEN_COUNTED = "isAppOpenCounted"

class SplashActivity :
    BaseInjectorActivity<SplashViewModel>(SplashViewModel::class.java) {

    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // To handle orientation change
        if (savedInstanceState?.getBoolean(IS_APP_OPEN_COUNTED, false) != true)
            viewModel.incrementAppOpenCount()

        observe(viewModel.appOpenCount) {
            binding.tvAppOpenCount.text = getString(R.string.app_open_count, it)
        }
        observe(viewModel.isNavigateFromSplash) {
            if (it) {
                startActivity(Intent(this, DashboardActivity::class.java))
                finish()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(IS_APP_OPEN_COUNTED, true)
    }
}