package com.poc.jokes.ui.jokes

import android.os.Bundle
import androidx.core.view.isVisible
import com.google.android.material.snackbar.Snackbar
import com.poc.jokes.R
import com.poc.jokes.databinding.FragmentJokesListBinding
import com.poc.jokes.entities.common.AppError
import com.poc.jokes.presentation.extensions.observe
import com.poc.jokes.presentation.jokes.JokesListViewModel
import com.poc.jokes.ui.base.BaseInjectorFragment

class JokesListFragment : BaseInjectorFragment<JokesListViewModel, FragmentJokesListBinding>(
    R.layout.fragment_jokes_list,
    FragmentJokesListBinding::bind,
    JokesListViewModel::class.java
) {

    override fun onViewBindingCreated(
        binding: FragmentJokesListBinding,
        savedInstanceState: Bundle?
    ) {
        super.onViewBindingCreated(binding, savedInstanceState)
        binding.rvJokes.adapter = JokesAdapter {
            navController.navigate(
                JokesListFragmentDirections.actionFragmentJokesListToJokeDetailsDialogFragment(
                    it
                )
            )
        }
        binding.refreshLayout.setOnRefreshListener {
            viewModel.loadJokes()
        }
        observe(viewModel.randomJokes) {
            binding.rvJokes.isVisible = it != null
            it?.let {
                (binding.rvJokes.adapter as JokesAdapter).submitList(it)
            }
        }
        observe(viewModel.error) {
            it?.let {
                showError(it.errorMessage, it.error)
            }
        }
        observe(viewModel.isLoading) {
            binding.refreshLayout.isRefreshing = it
        }
        observe(viewModel.isEmpty) {
            binding.tvError.isVisible = it
        }
    }

    private fun showError(errorMessage: String?, error: AppError) {
        binding?.root?.let {
            Snackbar.make(
                it,
                errorMessage ?: when (error) {
                    AppError.ConnectionError -> getString(R.string.error_connectivity)
                    AppError.ApiError,
                    AppError.GenericError,
                    AppError.UnknownError -> getString(R.string.error_loading_jokes)
                },
                Snackbar.LENGTH_SHORT
            ).apply {
                setAction(getString(R.string.general_label_retry)) {
                    viewModel.loadJokes()
                }
                show()
            }
        }
    }
}