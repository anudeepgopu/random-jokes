package com.poc.jokes.ui.dashboard

import android.os.Bundle
import com.poc.jokes.databinding.ActivityDashboardBinding
import com.poc.jokes.presentation.dashboard.DashboardViewModel
import com.poc.jokes.ui.base.BaseInjectorActivity

class DashboardActivity : BaseInjectorActivity<DashboardViewModel>(DashboardViewModel::class.java) {

    private lateinit var binding: ActivityDashboardBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDashboardBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}