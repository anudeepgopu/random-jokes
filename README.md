# README #

# Random Jokes

- Shows a list of 10 random jokes received from the API
- Refresh the list when pulled down to refresh
- Main a app counter and display on splash screen each tim the app is opened
- Show the splash screen for 3 seconds

## Architecture

The app is build based on Clean architecture using MVVM. As the architecture defines, the data flow happens from the innermost layer to the outermost layer. Inner layers cannot access data of outer layers.
Each layer of the architecture is split into a separate module, to allow scaling and maintainability. 
**Activity/Fragment->ViewModel->Usecases->Framework->Remote API|LocalDB(currently not added)**

**UI** - Activity/Fragment depends on ViewModel to get the jokes list that needs to be displayed. Only UI-specific operations are handled in the activity/fragment.

**ViewModel** - This holds jokes list that needs to be sent to the UI because it can automatically handle all lifecycle operations. Data is requested from Use cases.

**Usecases** - Each use-case handles only a specific function. It acts as a connector between the repository and View model while requesting data or performing any business logic.

**Framework** This forms the core layer for data since it has the repository. Repository fetches data from remote API. 

#### Asyncronous Calls

For doing asynchronous tasks we use Flows/coroutines which is the Google recommended way. We highly make use of this for listening to data changes or triggering any operations by observing.

## API Service
[JokeAPI ](https://jokeapi.dev/) is used to get list of random jokes.

### Project Setup ###

* No special instructions are required for project setup. 

## 3rd Party Libraries

| Library |  |
| ------ | ------ |
| Dagger2 | Used for dependency injection. It uses code generation using annotations rather than depending on reflections at runtime. Used to inject dependencies throughout the app into ViewModels, Activities, Usecases, Repositories, etc..|
| Retrofit2 | Used for Network operations to fetch data from Jokes API. |
| Moshi | JSON library to parse JSON objects by generating type adapters. Faster when compared to the widely used Gson library which uses reflections. |
| Mockito | Used to mock objects when executing unit tests. |
| Flexbox | Used to make recycler view flexible and display items depending on the space available |