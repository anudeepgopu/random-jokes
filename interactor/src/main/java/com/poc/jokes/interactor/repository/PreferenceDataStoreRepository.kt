package com.poc.jokes.interactor.repository

import kotlinx.coroutines.flow.Flow

interface PreferenceDataStoreRepository {

    fun geAppOpenCount(): Flow<Long>

    suspend fun incrementAppOpenCount()
}