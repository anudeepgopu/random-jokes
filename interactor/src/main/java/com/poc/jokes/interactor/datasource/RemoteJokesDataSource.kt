package com.poc.jokes.interactor.datasource

import com.poc.jokes.entities.common.AppResult
import com.poc.jokes.entities.remote.RandomJokesResponse

interface RemoteJokesDataSource {

    suspend fun getRandomJokes(): AppResult<RandomJokesResponse?>
}