package com.poc.jokes.interactor.repository

import com.poc.jokes.entities.common.AppResult
import com.poc.jokes.entities.remote.Joke
import kotlinx.coroutines.flow.Flow

interface JokesRepository {

    fun getRandomJokes(): Flow<AppResult<List<Joke>>>
}