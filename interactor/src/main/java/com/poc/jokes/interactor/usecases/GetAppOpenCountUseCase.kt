package com.poc.jokes.interactor.usecases

import com.poc.jokes.interactor.repository.PreferenceDataStoreRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetAppOpenCountUseCase @Inject constructor(
    private val preferenceDataStoreRepository: PreferenceDataStoreRepository
) {

    fun invoke(): Flow<Long> = preferenceDataStoreRepository.geAppOpenCount()
}