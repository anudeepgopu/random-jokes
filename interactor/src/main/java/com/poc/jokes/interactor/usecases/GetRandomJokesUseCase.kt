package com.poc.jokes.interactor.usecases

import com.poc.jokes.entities.common.AppResult
import com.poc.jokes.entities.remote.Joke
import com.poc.jokes.interactor.di.IoDispatcher
import com.poc.jokes.interactor.repository.JokesRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class GetRandomJokesUseCase @Inject constructor(
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
    private val jokesRepository: JokesRepository
) {

    fun invoke(): Flow<AppResult<List<Joke>>> =
        jokesRepository.getRandomJokes().flowOn(ioDispatcher)
}