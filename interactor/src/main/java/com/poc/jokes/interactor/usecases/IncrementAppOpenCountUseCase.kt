package com.poc.jokes.interactor.usecases

import com.poc.jokes.interactor.repository.PreferenceDataStoreRepository
import javax.inject.Inject

class IncrementAppOpenCountUseCase @Inject constructor(
    private val preferenceDataStoreRepository: PreferenceDataStoreRepository
) {

    suspend fun invoke() {
        preferenceDataStoreRepository.incrementAppOpenCount()
    }
}