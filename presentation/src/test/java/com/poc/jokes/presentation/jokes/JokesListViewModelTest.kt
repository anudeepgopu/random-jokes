package com.poc.jokes.presentation.jokes

import com.poc.jokes.entities.common.AppError
import com.poc.jokes.entities.common.AppResult
import com.poc.jokes.entities.remote.Joke
import com.poc.jokes.interactor.usecases.GetRandomJokesUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class JokesListViewModelTest {

    private val getRandomJokesUseCase: GetRandomJokesUseCase = mock()
    private val jokesResult = MutableStateFlow<AppResult<List<Joke>>>(AppResult.Loading())
    private lateinit var subject: JokesListViewModel

    @ExperimentalCoroutinesApi
    private val testDispatcher = TestCoroutineDispatcher()

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        whenever(getRandomJokesUseCase.invoke()).thenReturn(jokesResult)
        subject = JokesListViewModel(getRandomJokesUseCase)
    }

    @Test
    fun `loadJokes should return result to randomJokes`() {
        runBlockingTest {
            var result: List<Joke>? = null

            val job = launch {
                subject.randomJokes.collect {
                    result = it
                }
            }
            jokesResult.value = AppResult.Success(listOf())
            Assert.assertNotNull(result)
            job.cancel()
        }
    }

    @Test
    fun `loadJokes should return correct states`() {
        runBlockingTest {
            var isLoading: Boolean? = null
            var error: AppResult.Error<*>? = null
            var isEmpty: Boolean? = null

            val jobLoading = launch {
                subject.isLoading.collect {
                    isLoading = it
                }
            }
            val jobError = launch {
                subject.error.collect {
                    error = it
                }
            }
            val jobEmpty = launch {
                subject.isEmpty.collect {
                    isEmpty = it
                }
            }

            Assert.assertEquals(true, isLoading)
            Assert.assertEquals(false, isEmpty)
            Assert.assertNull(error)

            jokesResult.value = AppResult.Empty()
            Assert.assertEquals(false, isLoading)
            Assert.assertEquals(true, isEmpty)
            Assert.assertNull(error)

            jokesResult.value = AppResult.Error(null, AppError.UnknownError)
            Assert.assertEquals(false, isLoading)
            Assert.assertEquals(false, isEmpty)
            Assert.assertNotNull(error)

            jokesResult.value = AppResult.Success(listOf())
            Assert.assertEquals(false, isLoading)
            Assert.assertEquals(true, isEmpty)
            Assert.assertNull(error)

            jobLoading.cancel()
            jobError.cancel()
            jobEmpty.cancel()
        }
    }
}