package com.poc.jokes.presentation.jokes

import org.junit.Assert
import org.junit.Test


class JokeDetailsDialogViewModelTest {

    @Test
    fun `filter flags with true value`() {
        Assert.assertEquals(
            FLAG_3,
            JokeDetailsDialogViewModel().filterFlags(HashMap<String, Boolean?>().also {
                it[FLAG_1] = false
                it[FLAG_2] = null
                it[FLAG_3] = true
            })[0]
        )
    }

    @Test
    fun `filter flags return correct size`() {
        Assert.assertEquals(
            2,
            JokeDetailsDialogViewModel().filterFlags(HashMap<String, Boolean?>().also {
                it[FLAG_1] = true
                it[FLAG_2] = null
                it[FLAG_3] = true
            }).size
        )

        Assert.assertEquals(
            0,
            JokeDetailsDialogViewModel().filterFlags(HashMap<String, Boolean?>().also {
                it[FLAG_1] = null
                it[FLAG_2] = null
                it[FLAG_3] = false
            }).size
        )
    }

    companion object {

        const val FLAG_1 = "flag1"
        const val FLAG_2 = "flag2"
        const val FLAG_3 = "flag3"
    }
}