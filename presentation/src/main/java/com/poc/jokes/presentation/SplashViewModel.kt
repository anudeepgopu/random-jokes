package com.poc.jokes.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.poc.jokes.interactor.usecases.GetAppOpenCountUseCase
import com.poc.jokes.interactor.usecases.IncrementAppOpenCountUseCase
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val SPLASH_TIMER: Long = 3000// 3 seconds

class SplashViewModel @Inject constructor(
    getAppOpenCountUseCase: GetAppOpenCountUseCase,
    private val incrementAppOpenCountUseCase: IncrementAppOpenCountUseCase,
) : ViewModel() {

    val appOpenCount = getAppOpenCountUseCase.invoke()
    val isNavigateFromSplash = MutableStateFlow(false)

    init {
        viewModelScope.launch {
            delay(SPLASH_TIMER)
            isNavigateFromSplash.value = true
        }
    }

    fun incrementAppOpenCount() {
        viewModelScope.launch {
            incrementAppOpenCountUseCase.invoke()
        }
    }
}