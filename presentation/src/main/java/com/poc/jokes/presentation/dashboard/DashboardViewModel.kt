package com.poc.jokes.presentation.dashboard

import com.poc.jokes.presentation.base.BaseViewModel
import javax.inject.Inject


class DashboardViewModel @Inject constructor() : BaseViewModel()