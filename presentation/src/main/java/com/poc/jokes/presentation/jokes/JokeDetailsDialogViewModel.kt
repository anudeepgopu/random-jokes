package com.poc.jokes.presentation.jokes

import com.poc.jokes.presentation.base.BaseViewModel
import javax.inject.Inject


class JokeDetailsDialogViewModel @Inject constructor() : BaseViewModel() {

    fun filterFlags(flags: Map<String, Boolean?>?): List<String> =
        flags?.filterValues { it == true }?.map { it.key }.orEmpty()
}