package com.poc.jokes.presentation.base

import androidx.lifecycle.ViewModel
import com.poc.jokes.entities.common.AppResult
import kotlinx.coroutines.flow.MutableStateFlow

open class BaseViewModel : ViewModel() {

    val isLoading = MutableStateFlow(false)
    val isEmpty = MutableStateFlow(false)
    val error = MutableStateFlow<AppResult.Error<*>?>(null)

    fun setLoadingState() {
        isLoading.value = true
        isEmpty.value = false
        error.value = null
    }

    fun setEmptyState() {
        isLoading.value = false
        isEmpty.value = true
        error.value = null
    }

    fun setSuccessState() {
        isLoading.value = false
        isEmpty.value = false
        error.value = null
    }

    fun setError(value: AppResult.Error<*>) {
        isLoading.value = false
        isEmpty.value = false
        error.value = value
    }
}