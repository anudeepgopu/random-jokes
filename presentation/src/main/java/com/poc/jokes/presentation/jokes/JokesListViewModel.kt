package com.poc.jokes.presentation.jokes

import androidx.lifecycle.viewModelScope
import com.poc.jokes.entities.common.AppResult
import com.poc.jokes.entities.common.valueOrNull
import com.poc.jokes.entities.remote.Joke
import com.poc.jokes.interactor.usecases.GetRandomJokesUseCase
import com.poc.jokes.presentation.base.BaseViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject


class JokesListViewModel @Inject constructor(
    private val getRandomJokesUseCase: GetRandomJokesUseCase
) : BaseViewModel() {

    val randomJokes = MutableStateFlow<List<Joke>?>(null)

    init {
        loadJokes()
    }

    fun loadJokes() {
        viewModelScope.launch {
            getRandomJokesUseCase.invoke().onEach {
                when (it) {
                    is AppResult.Empty -> setEmptyState()
                    is AppResult.Error -> setError(it)
                    is AppResult.Loading -> setLoadingState()
                    is AppResult.Success -> setSuccessState()
                }
            }.filter { it is AppResult.Success }.collectLatest { result ->
                result.valueOrNull().let {
                    if (it.isNullOrEmpty()) {
                        setEmptyState()
                    } else {
                        randomJokes.value = it
                    }
                }
            }
        }
    }
}