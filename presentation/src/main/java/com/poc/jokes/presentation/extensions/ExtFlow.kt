package com.poc.jokes.presentation.extensions

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest

fun <T> AppCompatActivity.observe(state: Flow<T>, block: (T) -> Unit) {
    lifecycleScope.launchWhenStarted {
        state.collectLatest { block(it) }
    }
}

fun <T> Fragment.observe(state: Flow<T>, block: (T) -> Unit) {
    lifecycleScope.launchWhenStarted {
        state.collectLatest { block(it) }
    }
}
