package com.poc.jokes.presentation.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.poc.jokes.presentation.SplashViewModel
import com.poc.jokes.presentation.dashboard.DashboardViewModel
import com.poc.jokes.presentation.jokes.JokeDetailsDialogViewModel
import com.poc.jokes.presentation.jokes.JokesListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    internal abstract fun splashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DashboardViewModel::class)
    internal abstract fun dashboardViewModel(viewModel: DashboardViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(JokesListViewModel::class)
    internal abstract fun jokesListViewModel(viewModel: JokesListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(JokeDetailsDialogViewModel::class)
    internal abstract fun jokeDetailsDialogViewModel(viewModel: JokeDetailsDialogViewModel): ViewModel
}