package com.poc.jokes.entities.remote

enum class JokeType(val value: String?) {

    SINGLE("single"),
    TWO_PART("twopart"),
    UNKNOWN(null)
}