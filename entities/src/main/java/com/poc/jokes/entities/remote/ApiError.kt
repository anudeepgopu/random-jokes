package com.poc.jokes.entities.remote

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiError(val message: String?)
