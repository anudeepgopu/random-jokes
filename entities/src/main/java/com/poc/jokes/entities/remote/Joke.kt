package com.poc.jokes.entities.remote

import com.squareup.moshi.JsonClass
import java.io.Serializable

@JsonClass(generateAdapter = true)
data class Joke(
    val id: Int,
    val category: String?,
    val type: JokeType?,
    val joke: String?,
    val setup: String?,
    val delivery: String?,
    val flags: Map<String, Boolean>?,
    val safe: Boolean = false,
    val lang: String?
) : Serializable