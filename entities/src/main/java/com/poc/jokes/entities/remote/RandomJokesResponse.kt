package com.poc.jokes.entities.remote

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RandomJokesResponse(
    val error: Boolean?,
    val message: String?,
    val amount: Int?,
    val jokes: List<Joke>?
)