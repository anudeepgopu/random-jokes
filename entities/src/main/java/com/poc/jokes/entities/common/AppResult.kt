package com.poc.jokes.entities.common

sealed class AppResult<out T> {

    class Loading<T> : AppResult<T>()

    data class Success<T>(val value: T) : AppResult<T>()

    data class Error<T>(val errorMessage: String?, val error: AppError = AppError.GenericError) :
        AppResult<T>()

    class Empty<T> : AppResult<T>()
}

fun <T> AppResult<T>.valueOrNull(): T? {
    return if (this is AppResult.Success) {
        value
    } else {
        null
    }
}

fun <In, Out> AppResult<In>.castWithValue(value: Out?): AppResult<Out> {
    return when (this) {
        is AppResult.Empty -> AppResult.Empty()
        is AppResult.Error -> AppResult.Error(errorMessage, error)
        is AppResult.Success -> value?.let { AppResult.Success(it) } ?: AppResult.Empty()
        is AppResult.Loading -> AppResult.Loading()
    }
}