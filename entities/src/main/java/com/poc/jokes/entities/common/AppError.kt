package com.poc.jokes.entities.common

sealed class AppError {
    object ApiError : AppError()
    object ConnectionError : AppError()
    object UnknownError : AppError()
    object GenericError : AppError()
}